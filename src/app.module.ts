import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { ExpreinceModule } from './expreince/expreince.module';
import { SocialMediaModule } from './social_media/social_media.module';
import { EducationModule } from './education/education.module';
import { WorkModule } from './work/work.module';
import { SkillsModule } from './skills/skills.module';
import { ReleationsModule } from './releations/releations.module';
import { AuthModule } from './auth/auth.module';
import { OtpModule } from './otp/otp.module';
import { SequelizeModule } from '@nestjs/sequelize';


@Module({
  imports: [ConfigModule.forRoot({ envFilePath: '.env', isGlobal: true }), 
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      username: process.env.POSTGRES_USERNAME,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
      autoLoadModels: true,
      synchronize: true,
  }), MongooseModule.forRoot(process.env.MONGO_URI), UserModule, SocialMediaModule, ExpreinceModule, EducationModule, WorkModule, SkillsModule, ReleationsModule, OtpModule, AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
