import { CanActivate, Injectable, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const headers = request.headers.authorization;

        if (!headers) {
            throw new UnauthorizedException('Authorization header is missing');
        }

        const [type, token] = headers.split(' ');
        if (type !== 'Bearer' || !token) {
            throw new UnauthorizedException('Invalid token format');
        }

        try {
            const payload = await this.jwtService.verify(token, {
                secret: this.configService.get<string>('AccesTokenSecretKey'),
            });
            request.user = payload; // Attach payload to request
            return true;
        } catch (error) {
            throw new UnauthorizedException('Invalid token');
        }
    }
}
