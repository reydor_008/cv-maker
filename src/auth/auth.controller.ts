import { Controller, Post, Body, UseInterceptors } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { UserService } from 'src/user/user.service';
import { UserModule } from 'src/user/user.module';
import { LoginAuthDto } from './dto/login-auth.dto';
import { LoggingInterceptor } from './loggingInterseptor/loggingInterseptor';

@Controller('auth')
export class AuthController {
  constructor(private readonly userService: UserService) { }

  @Post('register')
  @UseInterceptors(LoggingInterceptor)
  async register(@Body() createUserDto: CreateUserDto) {
    return await this.userService.create(createUserDto);
  }

  @Post('login')
  async login(@Body() loginAuthDto: LoginAuthDto) {
    return await this.userService.login(loginAuthDto);
  }
}
