import { Module } from '@nestjs/common';
import { ReleationsService } from './releations.service';
import { ReleationsController } from './releations.controller';

@Module({
  controllers: [ReleationsController],
  providers: [ReleationsService],
})
export class ReleationsModule {}
