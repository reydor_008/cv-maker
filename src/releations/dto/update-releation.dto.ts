import { PartialType } from '@nestjs/mapped-types';
import { CreateReleationDto } from './create-releation.dto';

export class UpdateReleationDto extends PartialType(CreateReleationDto) {}
