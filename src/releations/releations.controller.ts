import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ReleationsService } from './releations.service';
import { CreateReleationDto } from './dto/create-releation.dto';
import { UpdateReleationDto } from './dto/update-releation.dto';

@Controller('releations')
export class ReleationsController {
  constructor(private readonly releationsService: ReleationsService) {}

  @Post()
  create(@Body() createReleationDto: CreateReleationDto) {
    return this.releationsService.create(createReleationDto);
  }

  @Get()
  findAll() {
    return this.releationsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.releationsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReleationDto: UpdateReleationDto) {
    return this.releationsService.update(+id, updateReleationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.releationsService.remove(+id);
  }
}
