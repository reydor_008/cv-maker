import { Injectable } from '@nestjs/common';
import { CreateReleationDto } from './dto/create-releation.dto';
import { UpdateReleationDto } from './dto/update-releation.dto';

@Injectable()
export class ReleationsService {
  create(createReleationDto: CreateReleationDto) {
    return 'This action adds a new releation';
  }

  findAll() {
    return `This action returns all releations`;
  }

  findOne(id: number) {
    return `This action returns a #${id} releation`;
  }

  update(id: number, updateReleationDto: UpdateReleationDto) {
    return `This action updates a #${id} releation`;
  }

  remove(id: number) {
    return `This action removes a #${id} releation`;
  }
}
