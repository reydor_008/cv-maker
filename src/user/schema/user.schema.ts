import { Table, Column, Model, DataType, PrimaryKey, Default } from 'sequelize-typescript';
import { v4 as UUIDV4 } from 'uuid';

@Table
export class User extends Model<User> {
    @PrimaryKey
    @Default(UUIDV4)
    @Column({
        type: DataType.UUID,
        allowNull: false,
    })
    id: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 50],
        },
    })
    first_name: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 50],
        },
    })
    last_name: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 100],
        },
    })
    address: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 50],
        },
    })
    city: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 20],
        },
    })
    postcode: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            len: [3, 20],
        },
    })
    phone: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            len: [5, 100],
        },
    })
    email: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    password: string;

    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    })
    isActive: boolean;

    @Column({
        type: DataType.UUID,
        allowNull: true,
    })
    avatar_id: string;
}
