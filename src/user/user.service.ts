import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schema/user.schema';
import { LoginAuthDto } from 'src/auth/dto/login-auth.dto';
import { hash, compare } from 'bcrypt';

@Injectable()
export class UserService {
  constructor(@InjectModel('Users') private userModel: Model<User>) { }

  async create(createUserDto: CreateUserDto): Promise<User | string> {
    return createUserDto.password
    const hashedPassword = await hash(createUserDto.password, 20);
    createUserDto.password = hashedPassword;
    const newUser = new this.userModel(createUserDto);
    await newUser.save();
    return newUser;
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find();
  }

  async login(loginAuthDto: LoginAuthDto): Promise<any> {
    const user = await this.userModel.findOne({ email: loginAuthDto.email });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    const isPasswordMatching = await compare(loginAuthDto.password, user.password);
    if (!isPasswordMatching) {
      throw new BadRequestException('Invalid credentials');
    }
    return { message: 'Login successful', userId: user._id };
  }

  async findOne(id: string): Promise<User> {
    const user = await this.userModel.findById(id);
    if (!user) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    return user;
  }

  async update(id: string, updateUserDto: Partial<UpdateUserDto>): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(id, updateUserDto, {
      new: true,
      runValidators: true,
    });
    if (!updatedUser) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    return updatedUser;
  }

  async remove(id: string): Promise<boolean> {
    const result = await this.userModel.findByIdAndDelete(id);
    return result ? true : false;
  }
}
