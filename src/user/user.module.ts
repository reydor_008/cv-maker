import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './user.controller';
import { User } from './schema/user.schema';

@Module({
  imports : [MongooseModule.forFeature([{name : User.name, schema : User}])],//MongooseModule.forFeature([{name : "Users", schema: UserSchema}])],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService,MongooseModule]
})
export class UserModule {}
