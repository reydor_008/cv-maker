import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
 
import { IsEmail, IsAlpha, Length, IsOptional } from 'class-validator';

export class UpdateUserDto {

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    first_name?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    last_name?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    adress?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    city?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    postcode?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    phone?: string;

    @IsOptional()
    @IsAlpha()
    @Length(3, 50)
    password?: string;

    @IsOptional()
    @Length(3, 50)
    @IsEmail()
    email?: string;
}
