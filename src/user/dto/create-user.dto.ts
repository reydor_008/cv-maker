import { IsEmail, IsString, IsNotEmpty, IsAlphanumeric, Length, IsMobilePhone } from "class-validator";

export class CreateUserDto {

    @IsString()
    @IsNotEmpty()
    @Length(3, 50)
    first_name: string;

    @IsString()
    @IsNotEmpty()
    @Length(3, 50)
    last_name: string;

    @IsString()
    @IsNotEmpty()
    @Length(3, 50)
    address: string;

    @IsString()
    @IsNotEmpty()
    @Length(3, 50)
    city: string;

    @IsString()
    @IsNotEmpty()
    @Length(3, 10)
    postcode: string;

    @IsMobilePhone()
    @IsNotEmpty()
    phone: string;

    @IsEmail()
    @IsNotEmpty()
    @Length(3, 50)
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(6, 50)
    password: string;
}
