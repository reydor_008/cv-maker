import { Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import { UserService } from './user.service';
import { LoggingInterceptor } from 'src/auth/loggingInterseptor/loggingInterseptor';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService){}
    @Get('/')
    @UseInterceptors(LoggingInterceptor)
    getAll(){
        return this.userService.findAll()
    }
}
