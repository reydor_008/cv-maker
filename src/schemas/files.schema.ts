import { UUIDV4 } from "sequelize";
import { Table, Column, Model, DataType, PrimaryKey, Default } from "sequelize-typescript";

@Table
export class Files extends Model<Files>{
    @PrimaryKey
    @Default(UUIDV4)
    @Column({
        type : DataType.UUID,
        allowNull : false
    })
    id: string

    @Column({
        type : DataType.STRING,
        allowNull : false
    })
    url : string
}