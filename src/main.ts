import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';



async function bootstrap() {
  const port = process.env.PORT || 3001
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
   transform: true,
    whitelist: true,
    forbidNonWhitelisted: true,
  }));
  await app.listen(port, () => { console.log('Port has been running on port ', port) });
}
bootstrap();
